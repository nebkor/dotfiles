;;; .emacs --- config file

;;; Commentary:

;;; Code:
(server-start)

(fringe-mode (cons 1 0))

;; Remove all trailing whitespace on save
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;; Changes all yes/no questions to y/n type
(fset 'yes-or-no-p 'y-or-n-p)

;; Use shift-arrows to navigate windows
(windmove-default-keybindings)

(setq echo-keystrokes 0.1)
(setq vc-follow-symlinks t)

(add-to-list 'load-path "~/.emacs.d/lisp/")
;; (load-library "my-functions.el")


;;(global-set-key [f1] 'find-thing-at-point)
(global-set-key [f4] 'kill-this-buffer)
(global-set-key [f5] 'save-buffer)
(global-set-key [f6] 'font-lock-fontify-buffer)
(global-set-key [f7] 'revert-buffer)

;; banish ui shits
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))

(setq custom-file "~/.emacs.d/custom.el")
(load custom-file)


(setq-default auto-fill-function 'do-auto-fill)
(setq-default fill-column 100)
(turn-on-auto-fill)
;; Disable auto-fill-mode in programming mode
(add-hook 'prog-mode-hook (lambda () (auto-fill-mode -1)))


;; Package shit
;;
;; Bootstrap straight.el
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;;;;  Effectively replace use-package with straight-use-package
;;; https://github.com/raxod502/straight.el/blob/develop/README.md#integration-with-use-package
(straight-use-package 'use-package)
(setq straight-use-package-by-default t)

;; OK, really minimal use-package; see https://github.crookster.org/switching-to-straight.el-from-emacs-26-builtin-package.el/
;;;;  package.el
;;; so package-list-packages includes them
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))


(use-package markdown-mode
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . gfm-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "pandoc"))

(straight-use-package 'flymake)

(use-package protobuf-mode
  )

(use-package which-key
  :init (which-key-mode))

(use-package solarized-theme
  )

(use-package yasnippet
  )

(use-package geiser
  :config
  (progn
    (add-hook 'scheme-mode-hook 'geiser-mode)
    (setq geiser-mode-smart-tab-p t)))

(use-package flycheck
  :init (global-flycheck-mode))

(use-package flycheck-pos-tip
  :requires (flycheck)
  :config
  (with-eval-after-load 'flycheck (flycheck-pos-tip-mode)))

(use-package smex
  :bind
  (([remap execute-extended-command] . smex)
   ("M-X" . smex-major-mode-commands)))

(use-package ido                        ; Better minibuffer completion
  :init (progn
          (ido-mode 1)
          (ido-everywhere 1))
  :config
  (progn
    (use-package flx-ido                    ; Flex matching for IDO
      :init (flx-ido-mode 1))

    (setq ido-enable-flex-matching t      ; Match characters if string doesn't match
          ido-create-new-buffer 'always   ; Create a new buffer if nothing matches
          ido-use-filename-at-point 'guess
          ;; Visit buffers and files in the selected window
          ido-default-file-method 'selected-window
          ido-default-buffer-method 'selected-window
          ido-use-faces nil)))             ; Prefer flx ido faces

(use-package company
  :init (global-company-mode)
  :config
  (progn
    ;; Use Company for completion
    (bind-key [remap completion-at-point] #'company-complete company-mode-map)

    (setq company-tooltip-align-annotations t
          ;; Easy navigation to candidates with M-<n>
          company-show-numbers t)
    (setq company-idle-delay 0)
    (setq company-minimum-prefix-length 1)
    (company-tng-configure-default)
    (setq company-frontends '(company-tng-frontend
                              company-pseudo-tooltip-frontend
                              company-echo-metadata-frontend)))
  :diminish company-mode)

(dolist (key '("<return>" "RET"))
  ;; Here we are using an advanced feature of define-key that lets
  ;; us pass an "extended menu item" instead of an interactive
  ;; function. Doing this allows RET to regain its usual
  ;; functionality when the user has not explicitly interacted with
  ;; Company.
  (define-key company-active-map (kbd key)
    `(menu-item nil company-complete
                :filter ,(lambda (cmd)
                           (when (company-explicit-action-p)
                             cmd)))))
(define-key company-active-map (kbd "TAB") #'company-complete-selection)
(define-key company-active-map (kbd "SPC") nil)

;; Company appears to override the above keymap based on company-auto-complete-chars.
;; Turning it off ensures we have full control.
(setq company-auto-complete-chars nil)

;; (use-package company-quickhelp          ; Documentation popups for Company
;;   :ensure t
;;   :defer t
;;   :init (add-hook 'global-company-mode-hook #'company-quickhelp-mode))


(use-package ibuffer                    ; Better buffer list
  :bind (([remap list-buffers] . ibuffer))
  ;; Show VC Status in ibuffer
  :config (setq ibuffer-formats
                '((mark modified read-only vc-status-mini " "
                        (name 18 18 :left :elide)
                        " "
                        (size 9 -1 :right)
                        " "
                        (mode 16 16 :left :elide)
                        " "
                        (vc-status 16 16 :left)
                        " "
                        filename-and-process)
                  (mark modified read-only " "
                        (name 18 18 :left :elide)
                        " "
                        (size 9 -1 :right)
                        " "
                        (mode 16 16 :left :elide)
                        " " filename-and-process)
                  (mark " "
                        (name 16 -1)
                        " " filename))))

(use-package hydra
  :init
  (defhydra hydra-zoom (global-map "<f2>")
    "zoom"
    ("g" text-scale-increase)
    ("l" text-scale-decrease)))

(use-package expand-region
  :bind ("C-=" . er/expand-region))

(use-package smartparens
  :init
  (progn
    ;;(load "~/.emacs.d/smartparens")
    (smartparens-global-mode 1)
    (show-smartparens-global-mode 1))
  :config
  (setq smartparens-strict-mode t))

;; rust shit
(use-package cargo
  )

(use-package flycheck-rust
  :requires (flycheck)
  :init (add-hook 'flycheck-mode-hook #'flycheck-rust-setup))

(use-package rust-mode
  :config
  (progn
    (require 'smartparens-rust)
    (add-hook 'rust-mode-hook 'cargo-minor-mode)
    (add-hook 'rust-mode-hook #'company-mode)
    (add-hook 'rust-mode-hook #'flycheck-rust-setup)
    (define-key rust-mode-map (kbd "C-+") #'lsp-rust-analyzer-expand-macro)
    (setq rust-format-on-save t)))

(use-package magit
  )

(use-package undo-tree
  :init (global-undo-tree-mode)
  :bind (("C-z" . undo)
         ("C-S-z" . redo)))

(use-package go-mode
  :config (define-key go-mode-map (kbd "M-.") #'godef-jump))

(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :bind (("M-." . xref-find-definitions))
  :hook ((rust-mode . lsp)
         (lsp-mode . lsp-enable-which-key-integration)
         (go-mode . lsp))
  :config (progn
            (setq lsp-prefer-flymake nil)
            (setq lsp-rust-server 'rust-analyzer)))

(defun lsp-go-install-save-hooks ()
  (add-hook 'before-save-hook #'lsp-format-buffer t t)
  (add-hook 'before-save-hook #'lsp-organize-imports t t))
(add-hook 'go-mode-hook #'lsp-go-install-save-hooks)

(use-package lsp-ui
  :requires lsp-mode flycheck
  :commands lsp-ui-mode
  :config
  (progn
    (define-key lsp-ui-mode-map [remap xref-find-definitions] #'lsp-ui-peek-find-definitions)
    (define-key lsp-ui-mode-map [remap xref-find-references] #'lsp-ui-peek-find-references)
    (setq lsp-ui-doc-use-childframe t
          lsp-ui-doc-position 'top
          lsp-ui-doc-include-signature t
          lsp-ui-sideline-enable nil
          lsp-ui-flycheck-enable nil
          ;;lsp-ui-flycheck-list-position 'right
          ;;lsp-ui-flycheck-live-reporting t
          lsp-ui-peek-enable t
          lsp-ui-peek-list-width 60
          lsp-ui-peek-peek-height 25))
  (add-hook 'lsp-mode-hook 'lsp-ui-mode))


(use-package company-lsp
  :commands company-lsp
  :config (progn
            (push 'company-lsp company-backends)
            ;; Disable client-side cache because the LSP server does a better job.
            (setq company-transformers nil
                  company-lsp-async t
                  company-lsp-cache-candidates nil)))

(use-package lsp-treemacs :commands lsp-treemacs-errors-list)


(provide '.emacs)

;;; -*-Emacs-Lisp-*-

;;; .emacs ends here
(put 'magit-diff-edit-hunk-commit 'disabled nil)
