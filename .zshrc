export PATH=$HOME/.local/bin:$HOME/.cargo/bin:~/bin:/usr/local/bin:/usr/bin:/sbin:/bin:/usr/sbin:/usr/games:/usr/X11R6/bin:/usr/local/sbin:~/larceny-mirror:~/git/nand2tetris/tools
export PAGER='less -FX'
export TZ='US/Pacific'
export CVS_RSH=ssh
export RSYNC_RSH=ssh
export ANDROID_HOME=${HOME}/android-sdk-linux
export ANDROID_PLATFORM=${ANDROID_HOME}/platforms/android-10
export KAWA_INSTALL_DIR=${HOME}/_kawa

export RUST_SRC_PATH=$(rustc --print sysroot)/lib/rustlib/src/rust/src

export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/git
#source /usr/local/bin/virtualenvwrapper.sh

setopt menu_complete
setopt correct
setopt nobeep
setopt notify

fpath+=/home/ardent/.zsh/Completion

bindkey -e

##### History!
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory
export DEBIAN_PREVENT_KEYBOARD_CHANGES=yes
bindkey '^[[A' up-line-or-search
bindkey '^[[B' down-line-or-search

limit coredumpsize 0
umask 022

#######  aliases
alias 1="setxkbmap dvorak"
alias 2="setxkbmap us"
alias 3="xmodmap ~/.xmodmap-kinesis"
#alias 4="xmodmap -e 'pointer = 3 2 1' ; xmodmap ~/kinesis.modmap"
alias u="sudo /etc/update-motd.d/90-updates-available ; /etc/update-motd.d/98-reboot-required"
alias rtest='raco test *-test.rkt'
alias hlog='git log --date-order --graph --date=short --format="%C(green)%h%Creset %C(yellow)%an%Creset %C(blue bold)%ad%Creset %C(red bold)%d%Creset%s"'
alias alog='git log --date-order --all --graph --date=short --format="%C(green)%h%Creset %C(yellow)%an%Creset %C(blue bold)%ad%Creset %C(red bold)%d%Creset%s"'
#alias tv="xrandr --output LVDS1 --auto --output HDMI1 --auto --right-of LVDS1"
alias fporn="firefox -no-remote -P private"
alias td=todo.sh
compdef td=todo.sh
alias notv="xrandr --output HDMI1 --off"
alias dq=dpkg-query
alias xmms=audacious
alias gmp="gmplayer -vo gl:yuv=2:rectangle=2"
alias fixmp3s="for i in *.mp3 ; do mv \$i \`echo \$i |sed 's/ /-/' |sed \"s/'//g\" |sed 's/&/+/g' |sed 's/ /_/g'\` ; done"
alias ig="grep -i"
alias mpf="mplayer -vo xv -cache 65536 -nobps -framedrop"
alias z="source ~/.zshrc"
alias t='eval `cat ~/.ssh/ssh-agent.out`'
alias ia="ls -aF --color"
alias i="ls -F --color"
alias irt='ls -Frt --color'
alias lla="ls -alFrt --color"
alias ll="ls -lFrt --color"
alias ssh-start='eval `ssh-agent | tee ~/.ssh/ssh-agent.out` ; ssh-add ~/.ssh/id_rsa'
alias ssh-stop='pkill ssh-agent ; rm ~/.ssh/ssh-agent.out'
alias ith='ls -Ft |head -15'
alias pi=nano
alias locate='/usr/bin/locate -i'
alias astat='hg status /home/ardent/ardent-embic'
alias pstat='hg status /home/ardent/pitythefoos'
alias rb="source ~/.rvm/scripts/rvm"
alias nv="source $NVM_DIR/nvm.sh"
alias f='(nohup firefox > /dev/null 2>&1)&'
alias more='less -FX'
alias serve='python -m SimpleHTTPServer 8000'

function kman
{
for i in `man -k $1 |awk '{print $1}' | grep $1`
	do
		man -k $i |grep "^$i[[:space:]]"
	done
}

PROMPT="%B%m%b:%2.%(#.#.>) "

READNULLCMD=less
REPORTTIME=100

setopt nohup

PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting

compinit

if [ -e /home/ardent/.nix-profile/etc/profile.d/nix.sh ]; then . /home/ardent/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer

