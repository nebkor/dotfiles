export DEBIAN_PREVENT_KEYBOARD_CHANGES=yes
#export CC=/usr/bin/clang-3.9
export NVM_DIR="$HOME/.nvm"
export TZ=US/Pacific
export WINIT_X11_SCALE_FACTOR=1.0
